CREATE TABLE IF NOT EXISTS node (
	node_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(100) NOT NULL,
	content VARCHAR(500) NOT NULL,
	parent_node_id INT,
	
	foreign key (parent_node_id) references node(node_id)
);