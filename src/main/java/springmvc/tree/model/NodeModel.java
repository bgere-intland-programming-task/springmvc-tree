package springmvc.tree.model;

import java.util.OptionalLong;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name="node")
public class NodeModel {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="node_id")
	private Long id;
	
	@Column(name="parent_node_id")
	private Long parentId;

	@NotBlank(message = "Node name must not be blank")
	private String name;
	
	@NotBlank(message = "Node content must not be blank")
	private String content;

	protected NodeModel() {}

	public NodeModel(String name, String content) {
		this.name = name;
		this.content = content;
	}
	
	public NodeModel(String name, String content, long parentNodeId) {
		this.name = name;
		this.content = content;
		this.parentId = parentNodeId;
	}

	@Override
	public String toString() {
		return String.format(
			"Node[id=%d, name='%s', content='%s']",
			this.id, this.name, this.content
		);
	}

	public Long getId() {
		return id;
	}
	
	public OptionalLong getParentNodeId() {
		if (this.parentId == null) {
			return OptionalLong.empty();
		}
		
		return OptionalLong.of(this.parentId);
	}
	
	public void setParentNodeId(Long parentNodeId) {
		this.parentId = parentNodeId;
	}
	
	public void removeParentNode() {
		this.parentId = null;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name= name;
	}

	public String getContent() {
		return content;
	}
	
	public void setContent(String content) {
		this.content = content;
	}
}
