package springmvc.tree.model;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface NodeRepository extends CrudRepository<NodeModel, Long> {
	@Query(nativeQuery = true, value = "select * from node where parent_node_id = ?1")
	public Iterable<NodeModel> findAllChildrenByNodeId(long nodeId);
}
