package springmvc.tree.controller;

import java.util.List;
import java.util.Optional;
import java.util.OptionalLong;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import springmvc.tree.controller.request.UpdateNodeRequest;
import springmvc.tree.controller.request.CreateNodeRequest;
import springmvc.tree.service.NodeNotFoundException;
import springmvc.tree.service.TreeService;

@RestController
public class NodeController {
	@Autowired
	private TreeService treeService;
	
	@GetMapping("/tree")
	public List<JsTreeRoot> getTree(@RequestParam("name") Optional<String> nameFilter) {
		if (nameFilter.orElse("").isBlank()) {
			return List.of(JsTreeRoot.fromTree(this.treeService.getTree()));
		}
		
		return List.of(JsTreeRoot.fromTree(this.treeService.getFilteredTreeByName(nameFilter.get())));
	}
	
	@GetMapping("/tree/nodes/{id}/content")
	public String loadContent(@PathVariable("id") long nodeId) throws NodeNotFoundException {
		var node = this.treeService
			.getNode(nodeId)
			.orElseThrow(() -> new NodeNotFoundException(nodeId));
		
		return node.getContent();
	}
	
	@PostMapping(path="/tree/nodes", consumes=MediaType.APPLICATION_JSON_VALUE)
	public JsTreeNodeWithoutChildren createNode(@RequestBody CreateNodeRequest request) throws NodeNotFoundException {
		var parentNodeId = request.getParentNodeId();
		if (parentNodeId.isEmpty()) {
			return new JsTreeNodeWithoutChildren(this.treeService.addTopLevelNode(request.getName(), request.getContent()));
		} 

		return new JsTreeNodeWithoutChildren(
			this.treeService.addNode(parentNodeId.getAsLong(), request.getName(), request.getContent())
		);
	}
	
	@PatchMapping(path="/tree/nodes/{id}", consumes=MediaType.APPLICATION_JSON_VALUE)
	public JsTreeNodeWithoutChildren updateNode(@PathVariable("id") long nodeId, @RequestBody UpdateNodeRequest request) throws NodeNotFoundException {
		return new JsTreeNodeWithoutChildren(this.treeService.updateNode(nodeId, request.getName(), request.getContent()));
	}
	
	@PutMapping(path="/tree/nodes/{id}/parent", consumes=MediaType.TEXT_PLAIN_VALUE)
	public JsTreeRoot reorganize(@PathVariable("id") long nodeId, @RequestBody String parentNodeId) throws NodeNotFoundException {
		this.treeService.moveSubTree(nodeId, OptionalLong.of(Long.valueOf(parentNodeId)));
		
		return JsTreeRoot.fromTree(this.treeService.getTree());
	}
	
	@DeleteMapping(path="/tree/nodes/{id}/parent")
	public JsTreeRoot moveToTop(@PathVariable("id") long nodeId) throws NodeNotFoundException {
		this.treeService.moveSubTree(nodeId, OptionalLong.empty());
		
		return JsTreeRoot.fromTree(this.treeService.getTree());
	}
	
	@DeleteMapping(path="/tree/nodes/{id}")
	public void deleteSubTree(@PathVariable("id") long nodeId) throws NodeNotFoundException {
		this.treeService.removeSubTree(nodeId);
	}
}
