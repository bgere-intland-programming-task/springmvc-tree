package springmvc.tree.controller;

import springmvc.tree.service.TreeNode;

public class JsTreeNodeWithoutChildren {
	private String id;
	private String text;
	private String parentId;
	
	public JsTreeNodeWithoutChildren(String id, String text, String parentId) {
		this.id = id;
		this.text = text;
		this.parentId = parentId;
	}

	public JsTreeNodeWithoutChildren(TreeNode treeNode) {
		this(
			String.valueOf(treeNode.getId()), 
			treeNode.getName(),
			treeNode.getParentId().isPresent() ? String.valueOf(treeNode.getParentId().getAsLong()) : JsTreeRoot.ROOT_ID
		);
	}
	
	public String getId() {
		return this.id;
	}
	
	public String getText() {
		return this.text;
	}	
	
	public String getParentId() {
		return this.parentId;
	}
}
