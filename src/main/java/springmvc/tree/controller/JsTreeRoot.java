package springmvc.tree.controller;

import springmvc.tree.service.RootTreeNode;

public class JsTreeRoot extends JsTreeNode {
	public static final String ROOT_ID = "root";
	
	public static JsTreeRoot fromTree(RootTreeNode tree) {
		var root = new JsTreeRoot(tree);
		tree.getChildren().forEach(treeNode -> root.addChild(JsTreeNode.fromTreeNodeRecursive(treeNode)));
		
		return root;
	}
	
	public JsTreeRoot(RootTreeNode rootTreeNode) {
		this(rootTreeNode.getName());
	}
	
	public JsTreeRoot(String text) {
		super(JsTreeRoot.ROOT_ID, text, null);
	}
}
