package springmvc.tree.controller;

import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import springmvc.tree.service.NodeNotFoundException;

@ControllerAdvice
public class NodeControllerExceptionHandler extends ResponseEntityExceptionHandler {
	@ExceptionHandler({NodeNotFoundException.class})
	protected ResponseEntity<Object> handleNodeNotFound(NodeNotFoundException ex, WebRequest request) {
		return this.handleExceptionInternal(ex, 
			"The specified tree node has not been found.", 
			new HttpHeaders(),
			HttpStatus.NOT_FOUND, 
			request
		);
	}
	
	@ExceptionHandler({ConstraintViolationException.class})
	protected ResponseEntity<Object> handleConstraintViolation(ConstraintViolationException ex, WebRequest request) {
		return this.handleExceptionInternal(ex, 
			ex.getConstraintViolations().stream().map(ConstraintViolation::getMessage).collect(Collectors.joining("\n")), 
			new HttpHeaders(), 
			HttpStatus.BAD_REQUEST, 
			request
		);
	}
}
