package springmvc.tree.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import springmvc.tree.service.FilteredTreeNode;
import springmvc.tree.service.TreeNode;

public class JsTreeNode extends JsTreeNodeWithoutChildren {
	private List<JsTreeNode> children;
	private JsTreeNodeData data;
	
	public static JsTreeNode fromTreeNodeRecursive(TreeNode treeNode) {
		var node = new JsTreeNode(treeNode);
		treeNode.getChildren().forEach(childTreeNode -> node.addChild(JsTreeNode.fromTreeNodeRecursive(childTreeNode)));
		
		return node;
	}
	
	public JsTreeNode(TreeNode treeNode) {
		super(treeNode);
		
		if (treeNode instanceof FilteredTreeNode) {
			this.data = new JsTreeNodeData(((FilteredTreeNode)treeNode).isFilteredOut());
		}
	}
	
	public JsTreeNode(String id, String text, String parentId) {
		super(id, text, parentId);
	}
	
	public JsTreeNodeData getData() {
		return this.data;
	}
	
	public List<JsTreeNode> getChildren() {
		if (this.children == null) {
			return Collections.emptyList();
		}
		
		return Collections.unmodifiableList(this.children);
	}
	
	public void addChild(JsTreeNode treeNode) {
		if (this.children == null) {
			this.children = new ArrayList<>();
		}
		
		this.children.add(treeNode);
	}
}
