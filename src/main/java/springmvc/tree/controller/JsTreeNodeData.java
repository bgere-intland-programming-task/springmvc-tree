package springmvc.tree.controller;

public class JsTreeNodeData {
	private boolean filteredOut = false;
	
	public JsTreeNodeData(boolean filteredOut) {
		this.filteredOut = filteredOut;
	}
	
	public boolean isFilteredOut() {
		return this.filteredOut;
	}
}
