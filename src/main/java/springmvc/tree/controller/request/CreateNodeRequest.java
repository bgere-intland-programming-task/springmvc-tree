package springmvc.tree.controller.request;

import java.util.OptionalLong;

import org.springframework.lang.NonNull;

public class CreateNodeRequest {
	private String name;
	private String content;
	private Long parentNodeId;
	
	@NonNull
	public String getName() {
		return this.name;
	}
	
	@NonNull
	public String getContent() {
		return this.content;
	}
	
	public OptionalLong getParentNodeId() {
		if (this.parentNodeId == null) {
			return OptionalLong.empty();
		}
		
		return OptionalLong.of(this.parentNodeId);
	}
}
