package springmvc.tree.controller.request;

import java.util.Optional;

public class UpdateNodeRequest {
	private String name;
	private String content;
	
	public Optional<String> getName() {
		return Optional.ofNullable(this.name);
	}
	
	public Optional<String> getContent() {
		return Optional.ofNullable(this.content);
	}
}
