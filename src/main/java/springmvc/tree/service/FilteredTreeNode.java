package springmvc.tree.service;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import springmvc.tree.model.NodeModel;

public class FilteredTreeNode extends TreeNode {
	private Predicate<TreeNode> filter;
	private Boolean filteredOut;
	private List<TreeNode> children;
	
	public FilteredTreeNode(NodeModel nodeModel, Predicate<TreeNode> filter) {
		super(nodeModel);
		
		this.filter = filter;
	}
	
	public FilteredTreeNode(long id, String name, String content, Predicate<TreeNode> filter) {
		super(id, name, content);
		
		this.filter = filter;
	}
	
	public boolean isFilteredOut() {
		if (this.filteredOut == null) {
			this.filteredOut = !this.filter.test(this);
		}
		
		return this.filteredOut;
	}
	
	@Override
	public Iterable<TreeNode> getChildren() {
		if (this.children == null) {
			var children = super.getChildren();
		
			this.children = StreamSupport.stream(children.spliterator(), false)
				.filter(childNode -> {
					if (!this.filter.test(childNode) && !childNode.getChildren().iterator().hasNext()) {
						return false;
					}
					
					return true;
				})
				.collect(Collectors.toList());
		}
		
		return this.children;
	}
	
	@Override
	public void addChildNode(TreeNode node) {
		super.addChildNode(node);
		
		this.children = null;
	}
}
