package springmvc.tree.service;

import java.util.Optional;
import java.util.OptionalLong;
import java.util.stream.StreamSupport;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import springmvc.tree.model.NodeModel;
import springmvc.tree.model.NodeRepository;

@Service
public class TreeService {
	private NodeRepository nodeRepository;
	
	@Autowired
	public TreeService(NodeRepository nodeRepository) {
		this.nodeRepository = nodeRepository;
	}
	
	public RootTreeNode getTree() {
		return StreamSupport.stream(this.nodeRepository.findAll().spliterator(), false)
			.collect(new TreeCollector());
	}
	
	public RootTreeNode getFilteredTreeByName(String nameFilter) {
		return StreamSupport.stream(this.nodeRepository.findAll().spliterator(), false)
			.collect(new FilteredTreeCollector(node -> node.getName().equals(nameFilter)));
	}
	
	public Optional<TreeNode> getNode(long nodeId) {
		return this.nodeRepository.findById(nodeId).map(TreeNode::new);
	}
	
	public TreeNode addTopLevelNode(String name, String content) {
		return this.internalAddNode(new NodeModel(name, content));
	}
	
	public TreeNode addNode(long parentId, String name, String content) throws NodeNotFoundException {
		this.nodeRepository
			.findById(parentId)
			.orElseThrow(() -> new NodeNotFoundException(parentId));
		
		return this.internalAddNode(new NodeModel(name, content, parentId));		
	}
	
	public TreeNode updateNode(long id, Optional<String> name, Optional<String> content) throws NodeNotFoundException {
		var node = this.nodeRepository
			.findById(id)
			.orElseThrow(() -> new NodeNotFoundException(id));
			
			
		name.ifPresent(value -> node.setName(value));
		content.ifPresent(value -> node.setContent(value));
		
		this.nodeRepository.save(node);
		
		return new TreeNode(node);
	}
	
	@Transactional
	public void removeSubTree(long subTreeRootNodeId) throws NodeNotFoundException {
		var node = this.nodeRepository
			.findById(subTreeRootNodeId)
			.orElseThrow(() -> new NodeNotFoundException(subTreeRootNodeId));
			
		this.deleteNodeRecursive(node);		
	}
	
	public void moveSubTree(long subTreeNodeId, OptionalLong newParentId) throws NodeNotFoundException {
		var node = this.nodeRepository.findById(subTreeNodeId)
			.orElseThrow(() -> new NodeNotFoundException(subTreeNodeId));
		
		if (newParentId.isPresent()) {
			this.nodeRepository.findById(newParentId.getAsLong())
				.orElseThrow(() -> new NodeNotFoundException(newParentId.getAsLong()));

			node.setParentNodeId(newParentId.getAsLong());
		} else {
			node.removeParentNode();
		}
		
		this.nodeRepository.save(node);
	}
	
	private TreeNode internalAddNode(NodeModel node) {
		this.nodeRepository.save(node);
		
		return new TreeNode(node);
	}
	
	private void deleteNodeRecursive(NodeModel node) {
		this.nodeRepository.findAllChildrenByNodeId(node.getId())
			.forEach(childNode -> this.deleteNodeRecursive(childNode));
		
		this.nodeRepository.delete(node);
	}
}
