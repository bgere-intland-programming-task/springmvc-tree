package springmvc.tree.service;

import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import springmvc.tree.model.NodeModel;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;

public class TreeCollector implements Collector<NodeModel, TreeCollector.Accumulator, RootTreeNode> {
	public static class Accumulator {
		private ArrayList<NodeModel> topLevelNodes;
		private HashMap<Long, List<NodeModel>> nodesByParentId;
		
		public void addNode(NodeModel node) {
			var parentId = node.getParentNodeId();
			if (parentId.isEmpty()) {
				if (this.topLevelNodes == null) {
					this.topLevelNodes = new ArrayList<>();
				}
				
				this.topLevelNodes.add(node);
			} else {
				if (this.nodesByParentId == null) {
					this.nodesByParentId = new HashMap<>();
				}
				
				if (this.nodesByParentId.containsKey(parentId.getAsLong())) {
					this.nodesByParentId.get(parentId.getAsLong()).add(node);
				} else {
					this.nodesByParentId.put(parentId.getAsLong(), new ArrayList<>(List.of(node)));
				}
			}
		}
		
		public List<NodeModel> getTopLevelNodes() {
			if (this.topLevelNodes == null) {
				return Collections.emptyList();
			}
			
			return Collections.unmodifiableList(this.topLevelNodes);
		}
		
		public Map<Long, List<NodeModel>> getNodesByParentId() {
			if (this.nodesByParentId == null) {
				return Collections.emptyMap();
			}
			
			return Collections.unmodifiableMap(this.nodesByParentId);
		}
	}
	
	@Override
	public BiConsumer<Accumulator, NodeModel> accumulator() {
		return (accumulator, node) -> {
			accumulator.addNode(node);
		};
	}
	
	@Override
	public BinaryOperator<Accumulator> combiner() {
		return (baseAcc, accToCombine) -> {
			accToCombine.getTopLevelNodes().forEach(node -> baseAcc.addNode(node));
			accToCombine.getNodesByParentId().forEach((nodeId, list) -> {
				list.forEach(node -> baseAcc.addNode(node));
			});
			
			return baseAcc;
		};
	}
	
	@Override
	public Supplier<Accumulator> supplier() {
		return () -> new Accumulator();
	}
	
	@Override
	public Function<Accumulator, RootTreeNode> finisher() {
		return accumulator -> {
			var root = new RootTreeNode();
			accumulator.getTopLevelNodes().forEach(node -> this.addChildNode(root, node));
		
			var unprocessedNodes = StreamSupport.stream(root.getChildren().spliterator(), false)
				.collect(Collectors.toCollection(ArrayDeque::new));
			
			while (!unprocessedNodes.isEmpty()) {
				var treeNode = unprocessedNodes.remove();
				var nodeId = treeNode.getId();
				
				if (accumulator.getNodesByParentId().containsKey(nodeId)) {
					accumulator.getNodesByParentId().get(nodeId).forEach(nodeModel -> {
						this.addChildNode(treeNode, nodeModel)
							.ifPresent(childTreeNode -> unprocessedNodes.add(childTreeNode));
					});
				}
			}
			
			
			return root;
		};
	}
	
	@Override
	public Set<Characteristics> characteristics() {
		return Set.of(Characteristics.UNORDERED);
	}
	
	protected Optional<? extends TreeNode> addChildNode(AbstractTreeNode node, NodeModel childNodeModel) {
		var treeNode = new TreeNode(childNodeModel);
		node.addChildNode(treeNode);
		
		return Optional.of(treeNode);
	}
}
