package springmvc.tree.service;

public class NodeNotFoundException extends Exception {
	private static final long serialVersionUID = 1L;

	public NodeNotFoundException(long nodeId) {
		super(String.format("The tree node with ID: %d has not been found.", nodeId));
	}
}