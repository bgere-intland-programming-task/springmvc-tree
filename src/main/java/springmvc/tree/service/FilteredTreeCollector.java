package springmvc.tree.service;

import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

import springmvc.tree.model.NodeModel;

public class FilteredTreeCollector extends TreeCollector {
	private Predicate<TreeNode> filter;
	
	public FilteredTreeCollector(Predicate<TreeNode> filter) {
		this.filter = filter;
	}
	
	@Override
	public Function<Accumulator, RootTreeNode> finisher() {
		return accumulator -> {
			var root = super.finisher().apply(accumulator);
			
			var filteredRoot = new RootTreeNode();
			
			root.getChildren().forEach(childNode -> {
				if (childNode instanceof FilteredTreeNode) {
					if (
						!((FilteredTreeNode)childNode).isFilteredOut()
						|| childNode.getChildren().iterator().hasNext()
					) {
						filteredRoot.addChildNode(childNode);
					}	
				}
			});
			
			return filteredRoot;
		};
	}
	
	@Override
	protected Optional<? extends TreeNode> addChildNode(AbstractTreeNode node, NodeModel childNodeModel) {
		var treeNode = new FilteredTreeNode(childNodeModel, this.filter);
		node.addChildNode(treeNode);
		
		return Optional.of(treeNode);
	}
}
