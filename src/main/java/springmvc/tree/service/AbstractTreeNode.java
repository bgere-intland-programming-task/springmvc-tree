package springmvc.tree.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public abstract class AbstractTreeNode {
	private List<TreeNode> children;
	
	public abstract String getName();
	
	public void addChildNode(TreeNode node) {
		if (this.children == null) {
			this.children = new ArrayList<TreeNode>();
		}
		
		this.children.add(node);
	}
	
	public Iterable<TreeNode> getChildren() {
		if (this.children == null) {
			return Collections.emptyList();
		}
		
		return Collections.unmodifiableList(this.children);
	}
	
	public Optional<TreeNode> findSubTree(long nodeId) {
		for (var node: this.getChildren()) {
			var subTree = node.findSubTree(nodeId);
			
			if (subTree.isPresent()) {
				return subTree;
			}
		}
		
		return Optional.empty();
	}	
}
