package springmvc.tree.service;

import java.util.Optional;
import java.util.OptionalLong;

import org.springframework.lang.NonNull;

import springmvc.tree.model.NodeModel;

public class TreeNode extends AbstractTreeNode {
	private long id;
	private String name;
	private String content;
	private OptionalLong parentId  = OptionalLong.empty();
	
	public TreeNode(NodeModel node) {
		this(node.getId(), node.getName(), node.getContent());
	}
	
	public TreeNode(long id, String name, String content) {
		this.id = id;
		this.name = name;
		this.content = content;
	}
	
	public Optional<TreeNode> findSubTree(long nodeId) {
		if (nodeId == id) {
			return Optional.of(this);
		}
		
		return super.findSubTree(nodeId);
	}
	
	public long getId() {
		return this.id;
	}
	
	public OptionalLong getParentId() {
		return this.parentId;
	}
	
	@NonNull
	public String getName() {
		return this.name;
	}
	
	@NonNull
	public String getContent() {
		return this.content;
	}
	
	@Override
	public void addChildNode(TreeNode node) {
		super.addChildNode(node);
		
		node.parentId = OptionalLong.of(this.id);
	}
}
