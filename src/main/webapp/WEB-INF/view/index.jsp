<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
    
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
    
<!DOCTYPE html>
<html style="position: relative; height: 100%; width: 100%">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<title>Spring MVC Tree</title>
	
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Fira+Sans:wght@400&display=swap" rel="stylesheet">

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
	<link rel="stylesheet" href="css/msgBoxLight.css" />
	
	<link rel="stylesheet" href="css/filterBox.css" />
	<link rel="stylesheet" href="css/floatButton.css" />
	<link rel="stylesheet" href="css/nodeForm.css" />
	<link rel="stylesheet" href="css/main.css" />
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/jstree.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.7/dist/loadingoverlay.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
	<script src="js/jquery.msgBox.js"></script>
	
	<script src="js/main.js"></script>
	<script src="js/tree.js"></script>
	<script src="js/filter.js"></script>
	<script src="js/content.js"></script>
	<script src="js/modal.js"></script>
</head>
<body style="position: relative; height: 100%; width: 100%; margin: 0">
	<div id="side-bar">
		<my:filter-box id="filter" />
		
		<div id="jstree">
		</div>
	</div>
		
	<div id="content">
	</div>
	
	<my:float-button id="add-button" type="add" target="node-modal" />

	<my:float-button id="delete-button" type="delete" />
	
	<my:float-button id="edit-button" type="edit" target="node-modal" />
	
	<my:modal id="node-modal">
		<div class="nf-name">
			<input name="name" type="text" placeholder="Name">
		</div>
		<div class="nf-content">
			<textarea name="content" placeholder="Content"></textarea>
		</div>
	</my:modal>
</body>
</html>