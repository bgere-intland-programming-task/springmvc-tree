<%@ tag language="java" pageEncoding="UTF-8" body-content="tagdependent" %>

<%@ attribute name="id" type="java.lang.String" required="true" %>
<%@ attribute name="cssClass" type="java.lang.String" %>

<div id="${id}" class="${empty cssClass ? 'modal' : cssClass}">
	<form class="node-form">
		<jsp:doBody />
		<div class="nf-buttons">
			<a class="nf-cancel" rel="modal:close">Cancel</a>
			<a class="nf-ok" rel="modal:close">OK</a>
		</div>
	</form>
</div>