<%@ tag language="java" pageEncoding="UTF-8" body-content="empty" %>

<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<%@ attribute name="id" type="java.lang.String" required="true" %>
<%@ attribute name="type" type="java.lang.String" required="true" %>
<%@ attribute name="target" type="java.lang.String"%>
<%@ attribute name="cssClass" type="java.lang.String" %>

<%@ variable name-given="faIconClassName" %>

<c:choose>
  <c:when test="${type eq 'add'}">
    <c:set var="faIconClassName" value="fa-plus"/>
  </c:when>
  <c:when test="${type eq 'delete'}">
    <c:set var="faIconClassName" value="fa-trash"/>
  </c:when>
   <c:when test="${type eq 'edit'}">
    <c:set var="faIconClassName" value="fa-pencil"/>
  </c:when>
</c:choose>

<a ${not empty target ? 'href="#'.concat(target).concat('"') : ''} id="${id}" class="${empty cssClass ? 'float-button' : cssClass}">
	<i class="fa ${faIconClassName}"></i>
</a>