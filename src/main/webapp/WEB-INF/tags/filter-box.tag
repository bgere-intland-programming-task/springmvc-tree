<%@ tag language="java" pageEncoding="UTF-8" body-content="empty"%>

<%@ attribute name="id" type="java.lang.String" required="true" %>
<%@ attribute name="cssClass" type="java.lang.String" %>
<%@ attribute name="tabindex" type="java.lang.Integer" %>

<form id="${id}" class="${empty cssClass ? 'filter-box' : cssClass}">
	<div tabindex="${empty tabIndex ? 0 : tabIndex}">
		<div class="fb-input">
			<input type="text" name="name" placeholder="Filter"/>
		</div>
		<div class="fb-clear-button">
			<i class="fa fa-close"></i>
		</div>
		<button type="submit" class="fb-search">
			<i class="fa fa-search"></i>
		</button>
	</div>
</form>