var SpringMvcTree = {};

$(function() {
	var content = new SpringMvcTree.Content('content');
	var nodeForm = new SpringMvcTree.EditNodeModal('node-modal');
	var filter = new SpringMvcTree.Filter('filter');
	var jstree = new SpringMvcTree.Tree({
		dom: 'jstree',
		url: 'tree',
		data: function () {
			return $('#filter').serialize();
		}
	});
	
	$('#jstree').on('rootselected', function () {
		content.unload();
	});
	
	$('#jstree').on('nodeselected', function (e, selectedNodeId) {
		content.loadFromUrl(['tree/nodes/', selectedNodeId, '/content'].join(''));
	});
	
	$('#add-button').on('click', function () {
		nodeForm.open({}, jstree.addNode);
	});
	
	$('#edit-button').on('click', function () {
		var data = jstree.getSelectedNode();
		
		data = {
			id: data.id,
			name: data.text,
			content: content.getContent()
		};
		
		nodeForm.open(data, jstree.editNode);
	})
	
	$('#delete-button').on('click', function () {
		var selectedNode = jstree.getSelectedNode();
		
		if (selectedNode === null) {
			return;
		}
		
		$.msgBox({
			title: "Delete",
			content: 'Are you sure you want to delete node: "' + selectedNode.text + '"?',
			type: "warning",
			opacity: 0.9,
			buttons: [{ value: "Yes" }, { value: "No" }],
			success: function (result) {
				if (result == "Yes") {
					jstree.deleteNode(selectedNode.id);
				}
			}
		});
	});
	
	$('#filter').on('filter', function () {
		jstree.refresh();
	});
	
	$('#content')
		.on('load', function () {
			$('#delete-button').removeClass('disabled');
			$('#edit-button').removeClass('disabled');
		})
		.on('init unload', function () {
			$('#delete-button').addClass('disabled');
			$('#edit-button').addClass('disabled');
		});
});