SpringMvcTree.EditNodeModal = function (dom) {
	var jqEl, nameEl, contentEl, callback, initialData;
		
	if (typeof dom === 'string') {
		jqEl = $('#' + dom);
	} else {
		jqEl = $(dom);
	}
	
	nameEl = jqEl.find('.nf-name input[type=text]');
	contentEl = jqEl.find('.nf-content textarea');
	
	jqEl.find('.nf-ok').on('click', function () {
		var form = jqEl.find('form'),
			data = initialData || {};
		
		$.each(form.serializeArray(), function(i, value) { data[value.name] = value.value; });
		
		callback(data);
	});
	
	this.open = function(data, submitCallback) {
		callback = submitCallback;
		
		jqEl.modal({
			clickClose: false,
			fadeDuration: 100
		});
		
		initialData = data;
		
		if (data) {
			if (data.name) {
				nameEl.val(data.name);
			} else {
				nameEl.val('');
			}
			
			if (data.content) {
				contentEl.val(data.content);
			} else {
				contentEl.val('');
			}
		}
	}
}