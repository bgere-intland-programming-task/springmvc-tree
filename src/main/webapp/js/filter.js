SpringMvcTree.Filter = function (dom) {
	var jqEl, inputEl;
	
	if (typeof dom === 'string') {
		jqEl = $('#' + dom);
	} else {
		jqEl = $(dom);
	}
	
	inputEl = jqEl.find('input');
	
	inputEl
		.on('focus', function () {
			jqEl.find('> div').addClass('focused');
		})
		.on('blur', function () {
			jqEl.find('> div').removeClass('focused');
		})
		.on('init keyup', function () {
			var text = $(this).val(),
				searchButton = jqEl.find('.fb-clear-button');
			
			if (text.length === 0) { 
				if (!searchButton.hasClass('disabled')) {
					searchButton.addClass('disabled');
				}
			} else {
				searchButton.removeClass('disabled');
			}
		});
	
	jqEl.submit(function (e) {
		e.preventDefault();

		jqEl.trigger('filter');
	});
	
	jqEl.find('.fb-clear-button').on('click', function () {
		var container = $(this).parents('.filter-box');
		
		if ($(this).hasClass('disabled')) {
			return;
		}
		
		inputEl.val('');
		inputEl.trigger('init');
		
		container.submit();
	});
	
	inputEl.trigger('init');
}