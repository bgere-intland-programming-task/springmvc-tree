SpringMvcTree.Tree = function (config) {
	var ROOT_ID = 'root',
		me = this,
		dom = config.dom,
		url = config.url,
		data = config.data,
		jqEl;
	
	if (typeof dom === 'string') {
		jqEl = $('#' + dom);
	} else {
		jqEl = $(dom);
	}
	
	jqEl.jstree({
		core : {
			check_callback: function (operation, node, nodeParent, nodePosition) {
				return nodeParent.id !== '#';
			},
			multiple : false,
			data : {
				url: url,
				data: data
			}
		},
		
	    plugins: [ "dnd" ]
	});
	
	jqEl.on('ready.jstree refresh.jstree', function (e, data) {
		var selected = data.instance.get_selected(),
			allNodes = jqEl.jstree().get_json(null, {flat: true}),
			rootNodeEl = jqEl.jstree().get_node(ROOT_ID, true).find('a').first(),
			rootFilteredOut = false;

		if (!selected || !selected.length) {
			jqEl.trigger('rootselected');
		}
		
		data.instance.open_all();
		
		$.each(allNodes, function (i, node) {
			var id = node.id,
				filteredOut = node.data.filteredOut,
				nodeEl = jqEl.jstree().get_node(id, true).find('a').first();
			
			if (filteredOut) {
				nodeEl.addClass('node-filtered');
				rootFilteredOut = true;
			} else {
				nodeEl.removeClass('node-filtered')
			}
		});
		
		if (rootFilteredOut) {
			rootNodeEl.addClass('node-filtered');
		} else {
			rootNodeEl.removeClass('node-filtered');
		}
	});
	
	jqEl.on('select_node.jstree', function (e, data) {
		var selectedNodeId = data.node.id;
		
		if (selectedNodeId === ROOT_ID) {
			jqEl.trigger('rootselected');
			
			return;
		}
		
		jqEl.trigger('nodeselected', selectedNodeId);
	});
	
	jqEl.on('move_node.jstree', function (e, data) {
		me.moveNode(data.node.id, data.node.parent);
	});
	
	function showError(response) {
		$.msgBox({
			title: "Error",
			content: response.responseText.replace('\n', '<br>'),
			type: "error",
			showButtons: false,
			opacity: 0.9,
			autoClose:true
		});
	}
	
	this.getSelectedNodeId = function() {
		var selectedNodeId = jqEl.jstree().get_selected();
		
		if (!selectedNodeId || !selectedNodeId.length || selectedNodeId[0] === ROOT_ID) {
			selectedNodeId = null;
		} else {
			selectedNodeId = parseInt(selectedNodeId[0], 10);
		}
		
		return selectedNodeId;
	};
	
	this.getSelectedNode = function() {
		var selectedNodeId = me.getSelectedNodeId();
		
		if (selectedNodeId === null) {
			return null;
		}
		
		return jqEl.jstree().get_node(selectedNodeId);
	};
	
	this.refresh = function () {
		jqEl.jstree().refresh();
	};
	
	this.addNode = function (data) {
		data.parentNodeId = me.getSelectedNodeId();
		
		$.post({
			url: 'tree/nodes',
			data: JSON.stringify(data),
			contentType: 'application/json'
		})
		.done(me.refresh)
		.fail(showError);
	};
	
	this.editNode = function (data) {
		var id = data.id;
		
		delete data.id;
		
		$.ajax({
			url: 'tree/nodes/' + id,
			type: 'patch',
			data: JSON.stringify(data),
			contentType: 'application/json'
		})
		.done(me.refresh)
		.fail(showError)
	}
	
	this.deleteNode = function (nodeId) {
		$.ajax({
			url: 'tree/nodes/' + nodeId,
			type: 'DELETE'
		})
		.done(me.refresh);
	}
	
	this.moveNode = function (nodeId, newParentId) {
		var ajaxSettings = {
			url: ['tree/nodes/', nodeId, '/parent'].join('')
		};
		
		if (newParentId === ROOT_ID) {
			ajaxSettings.type = 'DELETE';
		} else {
			ajaxSettings = {
				url: ajaxSettings.url,
				type: 'PUT',
				contentType: 'text/plain',
				data: newParentId
			}
		}
		
		$.ajax(ajaxSettings)
		.fail(showError)
		.always(me.refresh);
	}
}