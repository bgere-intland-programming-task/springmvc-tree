SpringMvcTree.Content = function (dom) {
	var me = this,
		text, jqEl;
	
	if (typeof dom === 'string') {
		jqEl = $('#' + dom);
	} else {
		jqEl = $(dom);
	}
	
	jqEl.trigger('init');
	
	function escapeHtml(unsafe) {
	    return unsafe
	         .replace(/&/g, "&amp;")
	         .replace(/</g, "&lt;")
	         .replace(/>/g, "&gt;")
	         .replace(/"/g, "&quot;")
	         .replace(/'/g, "&#039;");
	}

	
	this.unload = function () {
		text = '';
		
		jqEl.html('');
		jqEl.trigger('unload');
	}
	
	this.loadFromUrl = function (url) {
		var interval = window.setInterval(function () {
			jqEl.LoadingOverlay("show");
		}, 200);
		
		$.get(url)
			.done(function(content) {
				me.load(content);
			})
			.always(function() {
				window.clearInterval(interval);
				
				jqEl.LoadingOverlay("hide", true);
			});
	}
	
	this.load = function (content) {
		text = content;
		
		jqEl.html(escapeHtml(content).replace('\n', '<br/>'));
		jqEl.trigger('load');
	}
	
	this.getContent = function () {
		return text;
	}
}