# SpringMVC Tree

##  Build

The application is supported by the `Maven` build tool. 
To build a deployable package, after cloning the repository `cd` into the project base directory - where `mvnw` command is and run

```bash
./mvnw package
```
on Unix systems, or

```
mvnw.cmd package
```
on Windows.

## Run

You can run the application either locally by `Spring Boot` or by `Docker` or you can even deploy the `war` file to an application server like `Tomcat`

### 1. Docker

  To run the application by using `Docker`, you need 

   * the `docker` tool (version >=19.03.8) 
   * the `docker-compose` tool (version >=1.25.0).
   * and also you have to have an active account on https://hub.docker.com
   
  `cd` into the project base directory - where `docker-compose.yml` file is.
  
  Run
  
  ```bash
  docker-compose up
  ```
  
  The application will be available at: http://localhost:8080/springmvc-tree/
  
### 2. Spring Boot
  
  To run the application by`Spring Boot`, you will need
  
   * Java version >=11
   * a database directory must be available, which is `/db` by default. It can be overridden by the `spring.db.path` property.
     A `tree.db` H2 file database will be created here (user: sa, no password).

  `cd` into the project base directory and run
  
  ```bash
  java -Dspring.db.path=$HOME -jar target/springmvc-tree-0.0.1-SNAPSHOT.war
  ```

  The application will be available at: http://localhost:8080/
  
  